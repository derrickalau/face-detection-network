# Face-detection-network

developing...

# Overview
A activity social platform bassed on face-detection-app(https://github.com/Derrick-lau/Face-detection-app)
, built with React-Typescript and C# ASP.Net Core 3.1

# Features
- Clean Architecture using the CQRS + Mediator pattern

- Using Entity Framework to scaffold a database

- CRUD

# Setup & Installation
1. Run API project with net core 3.1
2. Run npm install in client-app project
3. Run npm start in client-app project

# Roadmap
- Adding Identity and Authentication using .Net Core Identity

- Using Redux as a state management library
 
- Implementing SignalR for real time web communication

- Implementing functionalities eg. Follow, photo upload

